x = 5
y = x
print(id(x))
print(id(y))
x += 1

print(id(x))

s = "real_python"

print(id(s))

s += "_rocks"

print(id(s))

my_list = [1, 2, 3]

print(id(my_list))

my_list.append(4)

print(id(my_list))

my_list[0] = 0

print(id(my_list))

x = 2337

y = x

print(id(x), id(y))

y += 1

print(id(x), id(y))

y = 2337

print(id(x), id(y))

x = 20
y = 19 + 1
print(id(x), id(y))

s1 = "realpythonaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!"

s2 = "realpythonaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!"

print(s1 is s2)